" CG: File  ~/.config/nvim/post-plugin.vim

" Use external executable to handle editorconfig files
let g:EditorConfig_exec_path = '/usr/bin/editorconfig'
let g:EditorConfig_core_mode = 'external_command'

" Buffer delete all others (delete all except current one)
command Bdo BufOnly
cabbrev bdo BuffOnly

" Visual diff command.
command Ldiffthis Linediff
cabbrev ldiffthis Linediff

command Ldiffoff Linediffreset
cabbrev ldiffoff LinediffReset

" Load colors! On the initial install this will error out, so make it silent
" so it installs without issues.
" silent! colorscheme monokai
silent! colorscheme jellybeans
" silent! colorscheme gruvbox

" Add a mapping for the NERDTree command, so you can just type :T to open
command T NERDTree

" Enable the powerline fonts.
let g:airline_powerline_fonts = 1

" Show the buffers at the top
let g:airline#extensions#tabline#enabled = 1

" Show the buffer numbers so I can `:b 1`, etc
let g:airline#extensions#tabline#buffer_nr_show = 1

" Aside from the buffer number, I literally just need the file name, so
" strip out extraneous info.
let g:airline#extensions#tabline#fnamemod = ':t'

" Set the theme for vim-airline
autocmd VimEnter * AirlineTheme powerlineish

let g:NERDTreeMouseMode = 3

" Use spaces instead just for yaml
autocmd Filetype yaml setl expandtab

" Highlighting on top of the error gutter is a bit overkill...
let g:ale_set_highlights = 0

" I want errors to be styled a bit more like neomake
let g:ale_sign_error = '✖'
highlight ALEErrorSign ctermfg=DarkRed ctermbg=NONE

" Same with warnings
let g:ale_sign_warning = '⚠'
highlight ALEWarningSign ctermfg=Yellow ctermbg=NONE

" Force the emoji to show up in the completion dropdown
let g:github_complete_emoji_force_available = 1

" Rely on tmux for the repl integration to work
let g:slime_target = 'tmux'

" This will ensure that the second panel in the current
" window is used when I tell slime to send code to the
" repl.
if exists("$TMUX")
	let s:socket_name = split($TMUX, ',')[0]

	let g:slime_default_config = {
					\ 'socket_name': s:socket_name,
					\ 'target_pane': ':.1'
					\ }
endif

" I dont like the default mappings of this plugin...
let g:slime_no_mappings = 1

" Ctrl+s will send the paragraph over to the repl.
nmap <c-s> <Plug>SlimeLineSend

" Ctrl+x will send the highlighted section over to the repl.
xmap <c-s> <Plug>SlimeRegionSend

" Make it so that ctrlp ignores files in .gitignore
let g:ctrlp_user_command = '(git status --short | awk "{ print \$2 }"; git ls-files -- %s) | sort -u'

function! g:AnsibleDocs() abort
	let word = expand('<cword>')
	new
	setlocal buftype=nofile
	setlocal noswapfile
	nnoremap <buffer> q :bd<cr>
	exe 'read ! ansible-doc' word
	set ft=man
	setlocal nomodifiable
endfunction

autocmd Filetype ansible.yaml nmap <buffer> <C-K> :call g:AnsibleDocs()<CR>

" Simple debugger integration.

fu! g:SendDBBreak()
	call system("tmux send-keys -t right 'sb(" . line('.') . ")\n'")
endfu

fu! g:SendDBCmd(cmd)
	call system("tmux send-keys -t right '" . a:cmd . "\n'")
endfu

au FileType javascript nnoremap <buffer> ,b :call g:SendDBBreak()<cr>
au FileType javascript nnoremap <buffer> ,s :call g:SendDBCmd('s')<cr>
au FileType javascript nnoremap <buffer> ,n :call g:SendDBCmd('n')<cr>
au FileType javascript nnoremap <buffer> ,c :call g:SendDBCmd('c')<cr>
au FileType javascript nnoremap <buffer> ,r :call g:SendDBCmd('r')<cr>
au FileType javascript nnoremap <buffer> ,R :call g:SendDBCmd('restart')<cr>

" Enable jsx by default as many projects use .js extension for jsx files.
let g:jsx_ext_required = 0



" CG -------------------------
"
"        _                          _   _   _
" __   _(_)_ __ ___        ___  ___| |_| |_(_)_ __   __ _ ___
" \ \ / / | '_ ` _ \ _____/ __|/ _ \ __| __| | '_ \ / _` / __|
"  \ V /| | | | | | |_____\__ \  __/ |_| |_| | | | | (_| \__ \
"   \_/ |_|_| |_| |_|     |___/\___|\__|\__|_|_| |_|\__, |___/
"                                                   |___/
" let mapleader = "\<Space>"
" let mapleader = " "
let mapleader = ","
let maplocalleader = ","


"-------------------- FUNCTION KEY MAPPINGS -------------------{
" -----NORMAL MODE MAPPINGS-----------
nnoremap <F1> :NERDTreeToggle<CR>        " Toogle NERDTree
set pastetoggle=<F2>
nnoremap <F3> :call ToggleWrapAndLinebreak()<CR>
nnoremap <S-F3> :ToggleLightBackground<CR>
nnoremap <F4> :call LanguageClient_contextMenu()<CR>
nnoremap <F5> :IndentSpacesToggle<CR>
nnoremap <F6> :ToggleColorColumn<CR>
nnoremap <F7> :UndotreeToggle<cr>
" nmap <F8> <Plug>(ale_fix)   " Bind F8 to fixing problems with ALE
nnoremap <S-F8> :ToggleALEFixOnSave<CR>
nnoremap <F9> :let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar><CR> <BAR>:echom 'Erased trailing whitespace !' <CR> " Remove all trailing whitespace (https://vim.fandom.com/wiki/Remove_unwanted_spaces) explicitly
nnoremap <F12> :echom expand('%:p:h')<CR>
nnoremap <S-F12> :echom expand('%:p')<CR>
" ---- COMMAND MODE MAPPINGS ----------
cnoremap <F12> <C-R>=expand('%:p:h')<CR>
cnoremap <S-F12> <C-R>=expand('%:p')<CR>
" ---- INSERT MODE MAPPINGS -----------
inoremap <F12> <C-R>=expand('%:p:h')<CR>
inoremap <S-F12> <C-R>=expand('%:p')<CR>
"-------------------- FUNCTION KEY MAPPINGS -------------------}


nnoremap ; :

" nnoremap 0 ^
" nnoremap <Space> $
" nnoremap B ^
" nnoremap E $

" Yank from the cursor to the end of the line, to be consistent with C and D.
nnoremap Y y$
" Visual select until end of line
nnoremap vv v$h
" select until end of line
" vmap <leader>E    $h
vmap $    $h

" map j to gj and k to gk, so line navigation ignores line wrap
" Treat long lines as break lines (useful when moving around in them)
nnoremap j gj
nnoremap k gk

nnoremap <leader>V ggVG
nnoremap <leader>A ggVG
" nnoremap <C-A> ggVG

" Tab through buffers (writes to them...)
nnoremap  <silent>   <tab>  :if &modifiable && !&readonly && &modified <CR> :write<CR> :endif<CR>:bnext<CR>
nnoremap  <silent> <s-tab>  :if &modifiable && !&readonly && &modified <CR> :write<CR> :endif<CR>:bprevious<CR>

" C-s to save file
noremap <silent> <C-s>          :update<CR>
vnoremap <silent> <C-s>         <C-C>:update<CR>
inoremap <silent> <C-s>         <ESC>:update<CR>


" Move visual selection 1-line up DOWN/UP
" shouldn't be mapped to j/k because by default those expand/shrink the selection linewise
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv   " Move visual selection up


" ####### Copy and Paste #######

" Copy only to clipboard register (@+)
" vnoremap <C-c>      "+y
" Copy first to primary selection register (@*), then copy from primary selection register to clipboard register (@+)
vnoremap <C-c>      "*y :let @+=@*<CR>

" Paste content of clipboard register (@+)
noremap <C-v>      "+p

" Use P in visual mode to repaste previously yanked content
" see https://stackoverflow.com/questions/18729972/vim-preserve-last-copied-text-when-copy-pasting-in-visual-mode
xnoremap <expr> P '"_d"'.v:register.'P'

" CG -------------------------
