 " Display images in the preview
 " Generate preview on buffer write (Example: on :w)

" There are a total of four values (0, 1, 2, 3) this option can take.

" Default: 0, which maps Control p (not a buffer write) to generating the preview and does not display images.
" let vim_markdown_preview_toggle=0

" Example: To display images with the hotkey mapping (defaults to Control p).

" let vim_markdown_preview_toggle=1

" Example: To display images automatically on buffer write.

" let vim_markdown_preview_toggle=2

" Example: To disregard images and still automatically preview on buffer write.

" let vim_markdown_preview_toggle=3

" The vim_markdown_preview_hotkey option

" By default, this plugin maps <C-p> (Control p) to activate the preview. To remap Control p to a different hotkey, change the binding. Don't forget to add the single quotation marks.

" Default: '<C-p>'

" Example: Mapping Control M.

let vim_markdown_preview_hotkey='<C-y>'

" The vim_markdown_preview_browser option

" By default, if you are using Unix, Google Chrome is the default browser. If you are on Mac OS X, Safari is the default. Note that bug #16 does not allow cross operating system and browser support. See the wiki page for more help.

" Default: 'Google Chrome'

" Example: Using Google Chrome.

" let vim_markdown_preview_browser='Google Chrome'
let vim_markdown_preview_browser='firefox'

" The vim_markdown_preview_temp_file option

" By default, this plugin keeps the rendered .html file. If you would automatically like to remove the html file after opening it in a browser, set this option to 1. Note that removing the rendered html file with a slow browser may err.

" Default: 0

" Example: Remove the rendered preview.

" let vim_markdown_preview_temp_file=1

" The vim_markdown_preview_github option

" If you prefer GitHub flavoured markdown you need to install Python grip. Note that this makes a request to GitHub's API (causing latencies) and may require authentication. This option also requires a network connection.

" Default: 0

" Example: Use GitHub flavoured markdown.

" let vim_markdown_preview_github=1

" The vim_markdown_preview_perl option

" If you prefer to use John Gruber's Markdown.pl to render HTML, set the following flag:

" Default: 0

" Example: Use Markdown.pl to render HTML.

" let vim_markdown_preview_perl=1

" The vim_markdown_preview_pandoc option

" If you prefer to use John MacFarlane's Pandoc to render HTML, set the following flag:

" Default: 0

" Example: Use Pandoc to render HTML.

" let vim_markdown_preview_pandoc=1

" The vim_markdown_preview_use_xdg_open option

" If your system does not come with see, and you would like to use xdg-open to view your rendered html in the browser, set the following flag:

" Default: 0

" Example: Use xdg-open.

let vim_markdown_preview_use_xdg_open=1

" Behind The Scenes

    " First, this plugin renders your markdown as html and creates a temporary html file.
    "     If image rendering is on, the html file will be in your working directory.
    "     Otherwise, it will be in /tmp/.
    " Next, this plugin either opens the html file or refreshes the Google Chrome or Safari tab.
    "     If you are using GitHub flavoured markdown, grip will make a call to the GitHub API and retrieve the html.
    " Lastly, if you choose, this plugin will remove the temporary file.

