" CG: File  ~/.config/nvim/init.vim _       _ _         _              __      _
" (_)_ __ (_) |___   _(_)_ __ ___    / /_   _(_)_ __ ___  _ __ ___
" | | '_ \| | __\ \ / / | '_ ` _ \  / /\ \ / / | '_ ` _ \| '__/ __|
" | | | | | | |_ \ V /| | | | | | |/ /  \ V /| | | | | | | | | (__
" |_|_| |_|_|\__(_)_/ |_|_| |_| |_/_/    \_/ |_|_| |_| |_|_|  \___|
"
" ##################### CG: Several mechanisms to override list of plugins to be loaded ####################
" List of plugins to be loaded can be defined in several ways on the command-line:
"
" EITHER
"       - Suppress any kind of mechanism to load plugins:
"         NVIM_DONT_LOAD_PLUGINS=true nvim
" OR
"       - Specify names of plugins via $NVIM_PLUGINS_LIST:
"           NVIM_PLUGINS_LIST="dhruvasagar/vim-zoom mhinz/vim-startify tpope/vim-surround vim-airline/vim-airline ChristianChiarulli/codi.vim" nvim
" OR
"       - Specify a plugin filename in $NVIM_PLUGINS_FILENAME:
"           NVIM_PLUGINS_FILENAME=$VIM_SCRIPTS_HOME/vim_plugins_for_codi.vim nvim -O $HOME/dotfiles/ruby_completion_testfile.rb
" ##########################################################################################################

" function! Src_cmd(file) abort
"   " If you're using a symlink to your script, but your resources are in
"   " the same directory as the actual script, you'll need to do this:
"   "   1: Get the absolute path of the script
"   "   2: Resolve all symbolic links
"   "   3: Get the folder of the resolved absolute file
"   let p = "source " . fnamemodify(resolve(expand('<sfile>:p')), ':h') . "/" . a:file
"   " exec p
"   return p
" endfunction

let s:dprefix = fnamemodify(resolve(expand('<sfile>:p')), ':h')
let s:prefix = "source " . s:dprefix . '/'


" Load plugins
call plug#begin("~/.config/nvim/plugged")
exec s:prefix . "plugin.vim"
call plug#end()


exec s:prefix . "post-plugin.vim"
autocmd Filetype gitcommit setl colorcolumn=72


if 0
"
"	~~ General Configurations ~~
"

" don't convert tabs to spaces
set noexpandtab

" Set tabs to take two character spaces
set tabstop=2

" Set how many characters indentation should be. This will ensure that you're
" using tabs, not spaces.
set shiftwidth=2

" Add the column number
set ruler

" Display the row numbers (line number)
set relativenumber

" Make the line number show up in the gutter instead of just '0'.
set number

" Add a bar on the side which delimits 80 characters.
set colorcolumn=80

" 72 characters makes it easier to read git log output.
autocmd Filetype gitcommit setl colorcolumn=72

" Will search the file as you type your query
set incsearch

set t_Co=256

" This will close the current buffer without closing the window
command Bd bp|bd #

" Enable clipboard. Can use x11 forwarding or socket mounting to
" make host clipboard accessible by the container.
set clipboard+=unnamedplus

" Using the blazing fast ag search tool for lgrep calls instead.
set grepprg=ag\ --nogroup\ --nocolor

" For some reason the mouse isn't enabled by default anymore...
set mouse=a

" Annnd, load the plugin-specific configurations.
" source ~/.config/nvim/post-plugin.vim
exec s:prefix . "post-plugin.vim"

" Folds start as opened instead of closed
set foldlevelstart=99

" Enable folds that are for the most part placed in the comments.
set foldmethod=marker

endif "if 0

"-------- CG --------
"        _                          _   _   _
" __   _(_)_ __ ___        ___  ___| |_| |_(_)_ __   __ _ ___
" \ \ / / | '_ ` _ \ _____/ __|/ _ \ __| __| | '_ \ / _` / __|
"  \ V /| | | | | | |_____\__ \  __/ |_| |_| | | | | (_| \__ \
"   \_/ |_|_| |_| |_|     |___/\___|\__|\__|_|_| |_|\__, |___/
"                                                   |___/
" if $VIM_SETTINGS_FILE == "" | let $VIM_SETTINGS_FILE ="$HOST_HOME/Documents/nvim/vim_settings.vim"  | endif
" if $VIM_SETTINGS_FILE == "" | let $VIM_SETTINGS_FILE = expand("~/.config/nvim/vim_settings.vim")  | endif

" if filereadable($VIM_SETTINGS_FILE)
"     " Load settings specified in file
"     silent exec "source " . $VIM_SETTINGS_FILE
"     " source $VIM_SETTINGS_FILE
" endif

" silent exec s:prefix . "vim_settings.vim"
exec s:prefix . "vim_settings.vim"
"-------- CG --------
